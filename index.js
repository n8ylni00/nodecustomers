const express = require('express');

const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

const port = 3000;

let customers = [
    { id: '1588323375416', firstName: 'John', lastName: 'Johnson', email: 'john@johnson.com', phone: '8233243' },
    { id: '1588323375417', firstName: 'Mary', lastName: 'Smith', email: 'mary@smith.com', phone: '6654113' },
    { id: '1588323375418', firstName: 'Peter', lastName: 'North', email: 'peter@north.com', phone: '901176' },
];

//Hae kaikki asiakkaat
app.get("/api/customers", (req, res) => {
    res.json(customers);
});

//Hae asiakas tunnuksella
app.get("/api/customers/:id", (req, res) => {
    const customerId = req.params.id;
    const customer = customers.filter(customer => customer.id === customerId);
    if (customer.length > 0)
        res.json(customer);
    else
        res.status(404).end();
})

//Lisää uusi asiakas
app.post("/api/customers", (req, res) => {
    //Pura asiakas pyynnön rungosta ja generoi tunnus. Lisätty .toString() jotta haku tunnuksella toimii.
    const newCustomer = {'id': Date.now().toString(), ...req.body};

    //Lisää uusi asiakas asiakas-arrayn loppuun
    customers = [...customers, newCustomer];

    res.json(newCustomer);
})

//Poista asiakas tunnuksella
app.delete("/api/customers/:id", (req, res) => {
    const id = req.params.id;
    customers = customers.filter(customer => customer.id !== id);
    res.status(204).end();
})

//Muokkaa asiakasta tunnuksella
app.put("/api/customers/:id", (req, res) => {
    const id = req.params.id;
    const updatedCustomer = {'id': id, ...req.body};

    //hae päivitetyn asiakkaan id
    const index = customers.findIndex(customer => customer.id === id);
    //päivitä asiakas taulukkoon
    customers.splice(index, 1, updatedCustomer);
    //lähetä päivitetyt asiakastiedot palautuksena
    res.json(updatedCustomer);
})

app.listen(port, () => {
   console.log(`Server is running on port ${port}.`);
});